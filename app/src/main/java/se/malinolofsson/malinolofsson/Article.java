package se.malinolofsson.malinolofsson;

/**
 * Class that holds the article model.
 * Created by Malin Olofsson on 03/09/16.
 */

public class Article {

    public String title;
    public String description;

    public Article(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }
}

package se.malinolofsson.malinolofsson;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * The Article adapter class
 *
 * Created by Malin Olofsson on 03/09/16.
 */
public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder> {

    private List<Article> articles;
    private ArrayList<Article> searchList = null;
    public Listener listener;

    interface Listener {
        void displayDetails(Article article);
    }

    /**
     * Constructor
     *
     * @param articles the list of articles.
     * @param listener MainActivity.
     */
    public ArticleAdapter(List<Article> articles, Listener listener) {
        this.articles = articles;
        this.listener = listener;
        this.searchList = new ArrayList<>();
        this.searchList.addAll(articles);
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        return new ArticleViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        holder.titleTextView.setText(articles.get(position).title);
        holder.articles = articles;
        holder.cardView.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public void filter(String searchString) {
        searchString = searchString.toLowerCase(Locale.getDefault());
        articles.clear();
        if (searchString.length() == 0) {
            articles.addAll(searchList);
        } else {
            for (Article article : searchList) {
                if (article.getTitle().toLowerCase(Locale.getDefault()).contains
                        (searchString)) {
                    articles.add(article);
                }
            }
        }
        notifyDataSetChanged();
    }
}

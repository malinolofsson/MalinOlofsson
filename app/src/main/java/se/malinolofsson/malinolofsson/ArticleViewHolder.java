package se.malinolofsson.malinolofsson;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

/**
 * The view holder for articles
 *
 * Created by Malin Olofsson on 03/09/16.
 */

public class ArticleViewHolder extends RecyclerView.ViewHolder {

    private final ArticleAdapter.Listener listener;

    public View cardView;
    public TextView titleTextView;
    public List<Article> articles;


    public ArticleViewHolder(View itemView, ArticleAdapter.Listener listener) {
        super(itemView);

        this.listener = listener;
        cardView = itemView;
        titleTextView = (TextView) itemView.findViewById(R.id.article_title);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int adapterPos = getAdapterPosition();
                if (adapterPos != RecyclerView.NO_POSITION) {
                    onItemClick(adapterPos);
                }
            }
        });
    }

    private void onItemClick(int adapterPos) {
        if (listener != null) {
            listener.displayDetails(articles.get(adapterPos));
        }
    }
}

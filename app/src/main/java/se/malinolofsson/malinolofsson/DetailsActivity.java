package se.malinolofsson.malinolofsson;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

/**
 * Display the article details
 */
public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String title = getIntent().getExtras().getString("article_title");
        String description = getIntent().getExtras().getString("article_description");

        TextView titleTextView = (TextView) findViewById(R.id.details_article_title);
        titleTextView.setText(title);

        TextView descriptionTextView = (TextView) findViewById(R.id.details_article_description);
        descriptionTextView.setText(description);
    }
}

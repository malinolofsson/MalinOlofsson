package se.malinolofsson.malinolofsson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements FetchXMLTask.Listener,
        ArticleAdapter.Listener {

    private static final String URL = "http://rss.nytimes.com/services/xml/rss/nyt/Sports.xml";

    private RecyclerView recyclerView;
    private ArticleAdapter articleAdapter;

    /**
     * Callbacks from FetchXMLTask and ArticleAdapter
     *
     * @param articles the list of articles.
     */
    @Override
    public void onPostExecute(List<Article> articles) {
        updateUI(articles);
    }

    @Override
    public void displayDetails(Article article) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("article_title", article.title);
        intent.putExtra("article_description", article.description);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void loadData() {
        new FetchXMLTask(this).execute(URL);
    }

    private void updateUI(List<Article> articles) {
        if (articles != null) {
            articleAdapter = new ArticleAdapter(articles, this);
            recyclerView.setAdapter(articleAdapter);
            setupSearchFilter();
        }
    }

    private void setupSearchFilter(){
        final EditText search = (EditText) findViewById(R.id.article_search);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                articleAdapter.filter(text);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Auto generated
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Auto generated
            }
        });
    }

}

package se.malinolofsson.malinolofsson;

import android.os.AsyncTask;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Fetch xml class
 *
 * Created by Malin Olofsson on 03/09/16.
 */
public class FetchXMLTask extends AsyncTask<Object, Object, List<Article>> {

    private static final String TAG = FetchXMLTask.class.getSimpleName();
    private Listener listener = null;

    interface Listener {
        void onPostExecute(List<Article> articles);
    }

    /**
     * Constructor
     *
     * @param listener MainActivity
     */
    public FetchXMLTask(Listener listener) {
        this.listener = listener;
    }

    @Override
    protected List<Article> doInBackground(Object... strings) {
        try {
            return loadXmlFromNetwork(strings[0]);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Error parsing xml");
            return null;
        }
    }

    private List<Article> loadXmlFromNetwork(Object urlString) throws XmlPullParserException,
            IOException {
        InputStream stream = null;
        XmlParser xmlParser = new XmlParser();

        List<Article> articles = null;

        try {
            stream = downloadUrl(urlString);
            articles = xmlParser.parse(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

        return articles;
    }

    @Override
    protected void onPostExecute(List<Article> articles) {
        if (listener != null) {
            listener.onPostExecute(articles);
        }
    }

    private InputStream downloadUrl(Object urlString) throws IOException {
        URL url = new URL((String) urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }

}
